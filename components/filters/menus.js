import React from 'react';
import { View, FlatList, Text, TouchableOpacity } from 'react-native';
import {GetWidth, Normalize, screenHeight} from "../../constants/size";

  function MenusFilter(props){
  return(
    <View style={{ marginHorizontal: '5%', height: screenHeight * 0.03, marginTop: screenHeight * 0.03, fontSize: Normalize(12) }}>
      <FlatList
        data={props.data}
        renderItem={( item ) =>{
          console.log(item)
          return (
          <TouchableOpacity key={item.index + item.item.text} style={{marginRight: GetWidth(10)}} >
            <Text style={{
              color: item.index === props.selected ? '#fff' : '#000',
              fontFamily: 'comfortaa',
              fontSize: 11
            }}>
              {item.item.text + '(' + item.item.value + ')'}
            </Text>
          </TouchableOpacity>
        )}}
        horizontal
        keyExtractor={(item, index) => index}
        extraData={props.data}
      />
    </View>
  )
}

export default MenusFilter