import React, { useState } from 'react';
import {View, TextInput, Image} from 'react-native';
import {GetHeight, GetWidth, screenHeight, screenWidth} from "../../constants/size";
import searchFilledIcon from "../../icons/search.png";

function Styles (values) {
  return(
    {
      container: {
        width: screenWidth * 0.7,
        height: GetHeight(45),
        borderRadius: GetHeight(40),
        backgroundColor: 'rgba(255, 255, 255, 0.3)',
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'row',
        paddingLeft: '3%'
      }
    }
  )
}

export default function SearchBar(props) {
  const [value, setValue] = useState(undefined);
  const styles = Styles(props);
  return(
    <View style={styles.container}>
      <Image source={searchFilledIcon} style={{ width: GetWidth(25), height: GetWidth(25), resizeMode: 'contain' }} />
      <TextInput
        value={props.value ? props.value : 'Search'}
        style={{
          fontFamily: 'comfortaa',
          color: '#fff',
          fontSize: 16,
          paddingLeft: '3%'
        }}
      />
    </View>
  )
}

