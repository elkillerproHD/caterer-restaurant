import { Platform, StyleSheet } from 'react-native';
import { isIphoneX } from "react-native-iphone-x-helper";
import { GetHeight, GetWidth, screenHeight, screenWidth } from "../../constants/size";
import {getStatusBarHeight} from "react-native-status-bar-height";

export default function Style() {
  return StyleSheet.create({
    container: {
      width: screenWidth,
      height: screenHeight,
      position: 'relative'
    },
    containerNavBar: {
      width: '100%',
      height: isIphoneX() ? GetHeight(85) : GetWidth(55),
      position: 'absolute',
      bottom: isIphoneX()
        ? 0
        : Platform.OS === 'ios'
          ? 0
          : getStatusBarHeight(),
      // bottom: isIphoneX() ? 0 : (Platform.OS === 'ios') ? 0 : GetHeight(20),
      left: 0
    },
    marginBottom: {
      height: 200
    }
  });
}
