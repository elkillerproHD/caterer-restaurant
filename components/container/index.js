import React from 'react';
import {
  View, Keyboard, TouchableWithoutFeedback
} from 'react-native';
import Style from './style';
import InputScrollView from "react-native-input-scroll-view";
// import BottomNavBar from '../../nav-bars/bottom';

class Container extends React.Component {
  static defaultProps = {
    headerText: 'header',
    navbarView: 1, // Array de 4, son los iconos, el valor de la prop es la seleccion del icono
    navBarActive: true,
    headerLeftOnPress: () => {},
    headerRightOnPress: false,
    headerRightIcon: false,
    headerRightMenuOnPress: false,
    scrollEnabled: false,
    scrollToInput: false,
    inputRef: undefined,
    buttonContainerStyle: {},
    dragStart: () => {},
    dragEnd: () => {},
  };

  state = {
    keyboardActive: false
  };

  styles = Style(this);

  componentDidUpdate(prevProps) {
    const { props, refs } = this;
    // Typical usage (don't forget to compare props):
    if (props.scrollToInput !== prevProps.scrollToInput) {
      if (props.scrollToInput) {
        const scrollResponder = refs.ScrollContainer.getScrollResponder();
        const scrollMove = props.inputRef;
        scrollResponder.scrollTo({ y: scrollMove, animated: true, x: 0, });
      } else {
        const scrollResponder = refs.ScrollContainer.getScrollResponder();
        scrollResponder.scrollTo({ y: 0, animated: true, x: 0, });
      }
    }
  }

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  keyboardDidShow = () => {
    this.setState({
      keyboardActive: true
    });
  };

  keyboardDidHide = () => {
    this.setState({
      keyboardActive: false
    });
  };

  DragStart = () => {
    this.props.dragStart();
  };

  DragEnd = () => {
    this.props.dragEnd();
  };

  render() {
    const { props, styles, state } = this;
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          Keyboard.dismiss();
        }}
        accessible={false}
        disabled={!state.keyboardActive}
      >
        <View style={this.styles.container}>
          {props.topContainer}
          {
            props.scrollEnabled
              ? (
                <InputScrollView
                  keyboardShouldPersistTaps="always"
                  ref="ScrollContainer"
                  onScrollBeginDrag={this.DragStart}
                  onScrollEndDrag={this.DragEnd}
                  keyboardOffset={100}
                >
                  {props.children}
                  <View style={styles.marginBottom} />
                </InputScrollView>
              )
              : (
                <View>
                  {props.children}
                  <View style={styles.marginBottom} />
                </View>
              )
          }
          {
            props.navBarActive
            && (
              <View style={styles.containerNavBar}>
              </View>
            )
          }
        </View>
      </TouchableWithoutFeedback>

    );
  }
}


export default Container;
