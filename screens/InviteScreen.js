import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from "react-native";
import Container from "../components/container";
import {GetHeight, GetWidth, Normalize, screenHeight, screenWidth} from "../constants/size";
import {getStatusBarHeight} from "react-native-status-bar-height";
import LinearGradient from 'react-native-linear-gradient';
import StarIcon from "../icons/star.png";

const strings = {
  title: 'Holiday Event',
  address: '1045! W Granger Ave, Boise',
  youInvited: 'You\'re, invited',
  text: 'I\'m hosting a social event using this caterer and I wish you to attend. The caterer\'s restaurant is shown above but the event will be hosted at my home. Please contact me whether you can attend or not.',
  invite: 'INVITE'
};

export default function InviteScreen(props) {
  /**
   * Go ahead and delete ExpoConfigView and replace it with your content;
   * we just wanted to give you a quick view of your config.
   */
  function BackPress(){
    console.log()
    props.navigation.navigate('Select');
  }
  return (
    <Container>
      <View style={styles.container}>
        <View style={styles.background}>
          <View>
            <Image style={{ width: screenWidth, height: screenHeight * 0.4 }} source={{uri: 'https://www.hoteldesroches.com/uploads/crop/article/restaurants-paradisier-salle1-600x_.jpg'}} />
          </View>
          <LinearGradient colors={['#bfbfbf', '#bfbfbf', '#adadad','#adadad', '#414141']} style={{
            width: screenWidth,
            height: screenHeight * 0.6,
            elevation: 4,
            zIndex: 2
          }}>
            <View style={{paddingHorizontal: '5%'}}>
              <Text style={{color: '#fff', marginTop: screenHeight * 0.02, fontSize: Normalize(22), fontFamily: 'comfortaa-bold'}}>{ strings.title }</Text>
              <Text style={{color: '#fff', marginTop: screenHeight * 0.02, fontSize: Normalize(14), fontFamily: 'comfortaa-bold'}}>{ strings.address }</Text>
              <Text style={{color: '#fff', marginTop: screenHeight * 0.03, fontSize: Normalize(14), fontFamily: 'comfortaa-bold'}}>{ strings.youInvited }</Text>
              <Text style={{color: '#fff', marginTop: screenHeight * 0.015, fontSize: Normalize(12), fontFamily: 'comfortaa-bold'}}>{ strings.text }</Text>
            </View>
          </LinearGradient>
          <View style={{
            width: screenWidth * 1.2,
            height: screenHeight * 0.05,
            backgroundColor: "#bfbfbf",
            position: 'absolute',
            left: 0,
            top: screenHeight * 0.4,
            shadowColor: "#bfbfbf",
            shadowOffset: {
              width: -15,
              height: -25,
            },
            shadowOpacity: 0.98,
            shadowRadius:8,
            elevation: 3,
            zIndex: 1
          }} />
          <View style={{
            width: screenWidth,
            position: 'absolute',
            right: 0,
            bottom: screenHeight * 0.055,
            height: GetWidth(45),
            zIndex: 3,
            flexDirection: 'row',
            justifyContent: 'flex-end',
            alignItems: 'center'
          }}>
            <TouchableOpacity style={{
              width: GetWidth(135),
              height: GetWidth(55),
              backgroundColor: '#ECB34B',
              borderRadius: GetWidth(55),
              marginRight: screenWidth * 0.02,
              justifyContent: 'center',
              alignItems: 'center',
              paddingLeft: '5%'
            }}>
              <Text style={{textAlign: 'left', width: '100%', color: '#fff', fontSize: Normalize(22), fontFamily: 'comfortaa-bold'}}>{ strings.invite }</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{
              width: GetWidth(55),
              height: GetWidth(55),
              backgroundColor: '#ECB34B',
              borderRadius: GetWidth(55),
              marginRight: screenWidth * 0.05,
              justifyContent: 'center',
              alignItems: 'center'
            }}>
              <Image source={StarIcon} style={styles.starIcon} />
            </TouchableOpacity>
          </View>
          <TouchableOpacity onPress={BackPress} style={{ position: 'absolute', top: screenHeight * 0.05, left: screenWidth * 0.05 }} >
            <Text style={{ fontSize: 20, color: '#fff', fontWeight: 'bold' }} >BACK</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Container>
  );
}

const styles = StyleSheet.create({
  container: {
    width: screenWidth,
    height: screenHeight,
    backgroundColor: '#fff',
    paddingTop: getStatusBarHeight(),
    justifyContent: 'center',
    alignItems: 'center'
  },
  background: {
    height: screenHeight - getStatusBarHeight() - screenHeight * 0.01,
    width: screenWidth - screenWidth * 0.02,
    borderRadius: screenWidth * 0.1,
    overflow: 'hidden',
    position: 'relative'
  },
  starIcon: {
    width: GetWidth(30),
    height: GetWidth(30),
    resizeMode: 'contain'
  },
});
