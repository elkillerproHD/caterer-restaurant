import React, { useState } from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Text
} from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import Container from "../components/container";
import {GetWidth, screenHeight, screenWidth} from "../constants/size";
import SearchBar from "../components/searchBar";
import StarIcon from "../icons/star.png"
import StarFilledIcon from "../icons/starFilled.png"
import MenusFilter from "../components/filters/menus";
import Carousel, { Pagination } from "react-native-snap-carousel";
import StarBlack from '../icons/star-black.png'

const carrouselStateInit = 0;
const hardCodeImage = 'https://t1.rg.ltmcdn.com/es/images/0/1/9/img_huevos_fritos_con_bacon_6910_600.jpg'

const filters = [
  {
    text: "STARTERS",
    value: 18,
  },
  {
    text: "SMALL BITES",
    value: 17,
  },
  {
    text: "SALADS",
    value: 10,
  },
  {
    text: "PIZZA",
    value: 16,
  },
  {
    text: "PASTA",
    value: 9,
  },
  {
    text: "SOUP",
    value: 5,
  },
  {
    text: "SANDWICH",
    value: 9,
  },
];

const hardCodeData = [{},{},{},{},{},{}];

export default function Menus(props) {

  function BackPress(){
    console.log(props)
    props.navigation.navigate('Select');
  }

  const [ starState, setStarState ] = useState(false);
  const [ filterState, setFilterState ] = useState(1);
  const [ carrouselState, setCarrouselState ] = useState(carrouselStateInit);
  function SwitchStar(){
    setStarState(!starState);
  }
  return (
    <Container>
      <View style={styles.container}>
        <View style={styles.background}>
          <View style={styles.searchBarStarContainer}>
            <SearchBar />
            <TouchableOpacity style={styles.starContainer} activeOpacity={1} onPress={SwitchStar}>
            {
              starState
              ? <Image source={StarFilledIcon} style={styles.starIcon} />
              : <Image source={StarIcon} style={styles.starIcon} />
            }
            </TouchableOpacity>
          </View>
          <MenusFilter selected={filterState} data={filters} />
          <Text style={{
            marginLeft: '5%',
            marginTop: '5%',
            fontSize: 14,
            color: '#fff',
            fontFamily: 'comfortaa',
          }}>{filters[filterState].text}</Text>
          <View style={{
            width: '100%',
            height: screenHeight * 0.75,
            paddingTop: '10%',
          }}>
            <Carousel
              ref={c => this._slider1Ref = c}
              data={hardCodeData}
              renderItem={(item) => {
                console.log(item);
                return(
                <View key={item.item.text + 'Slider'} style={{flex: 1}}>
                  <View style={{ height: '55%', padding: '2%', backgroundColor: '#fff' }}>
                    <Image style={{flex: 1, borderRadius: screenWidth * 0.05}} source={{uri: 'https://t1.rg.ltmcdn.com/es/images/0/1/9/img_huevos_fritos_con_bacon_6910_600.jpg'}} />
                  </View>
                  <View style={{ position: 'relative', height: '45%', backgroundColor: '#fff', paddingLeft: '6%', paddingTop: '3%' }}>
                    <Text style={{fontSize: 18, fontFamily: 'comfortaa-bold'}}>Egg and Bacon</Text>
                    <Text style={{fontSize: 16, marginTop: '5%', fontFamily: 'comfortaa'}}>The Egg and Bacon in the breakfast is the best for start the day</Text>
                    <Image source={StarBlack} style={{ ...styles.starIcon, position: 'absolute', bottom: '5%', right: '5%' }} />
                  </View>
                </View>
              )}}
              sliderWidth={screenWidth}
              itemWidth={screenWidth * 0.65}
              hasParallaxImages={true}
              firstItem={carrouselState}
              inactiveSlideScale={0.94}
              inactiveSlideOpacity={0.7}
              // inactiveSlideShift={20}
              containerCustomStyle={styles.slider}
              contentContainerCustomStyle={{}}
              loop={false}
              loopClonesPerSide={2}
              autoplay={true}
              autoplayDelay={500}
              autoplayInterval={3000}
              onSnapToItem={(index) => {setCarrouselState(index)} }
            />
            <Pagination
              dotsLength={hardCodeData.length}
              activeDotIndex={carrouselState}
              containerStyle={{justifyContent: 'flex-start', paddingLeft: screenWidth * 0.16}}
              dotColor={'rgba(255, 255, 255, 0.92)'}
              dotStyle={{}}
              inactiveDotColor={'rgba(255, 255, 255, 0.85)'}
              inactiveDotOpacity={0.4}
              inactiveDotScale={0.6}
              carouselRef={this._slider1Ref}
              tappableDots={!!this._slider1Ref}
            />
          </View>
          <TouchableOpacity onPress={BackPress} style={{ position: 'absolute', top: screenHeight * 0.18, right: screenWidth * 0.05, zIndex:2, elevation: 2 }} >
            <Text style={{ fontSize: 20, color: '#fff', fontWeight: 'bold' }} >BACK</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Container>

  );
}

Menus.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({
  container: {
    width: screenWidth,
    height: screenHeight,
    backgroundColor: '#fff',
    paddingTop: getStatusBarHeight(),
    justifyContent: 'center',
    alignItems: 'center'
  },
  background: {
    height: screenHeight - getStatusBarHeight() - screenHeight * 0.01,
    width: screenWidth - screenWidth * 0.02,
    borderRadius: screenWidth * 0.1,
    backgroundColor: '#bfbfbf',
    overflow: 'hidden'
  },
  searchBarStarContainer: {
    width: '100%',
    paddingHorizontal: '5%',
    paddingTop: '8%',
    flexDirection: 'row',
    alignItems: 'center'
  },
  starIcon: {
    width: GetWidth(30),
    height: GetWidth(30),
    resizeMode: 'contain'
  },
  starContainer: {
    marginLeft: screenWidth * 0.08
  }
});
