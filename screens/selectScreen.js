import React from 'react';
import { View, TouchableOpacity, Text} from "react-native";
import {GetHeight, screenHeight, screenWidth} from "../constants/size";

export function SelectScreen(props){

  function GoToInvite(){
    props.navigation.navigate('Invite')
  }
  function GoToDetails(){
    props.navigation.navigate('Details')
  }
  function GoToMenus(){
    props.navigation.navigate('Menus')
  }

  return(
    <View style={{
      width: screenWidth,
      height: screenHeight,
      backgroundColor: '#fff',
      paddingLeft: '5%',
      paddingTop: screenHeight * 0.3
    }}>
      <Text style={{ fontSize: 22, marginBottom: GetHeight(10) }} >Select screen:</Text>
      <TouchableOpacity onPress={GoToInvite} style={{marginVertical: GetHeight(5)}}>
        <Text style={{ fontSize: 16 }} >Invite</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={GoToDetails} style={{marginVertical: GetHeight(5)}}>
        <Text style={{ fontSize: 16 }} >Details</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={GoToMenus} style={{marginVertical: GetHeight(5)}}>
      <Text style={{ fontSize: 16 }} >Menus</Text>
    </TouchableOpacity>
    </View>
  )
}