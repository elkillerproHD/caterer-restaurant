import React from 'react';
import {Image, StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import Container from "../components/container";
import {GetHeight, GetWidth, screenHeight, screenWidth} from "../constants/size";
import {getStatusBarHeight} from "react-native-status-bar-height";
import SearchBar from "../components/searchBar";
import StarFilledIcon from "../icons/starFilled.png";
import StarIcon from "../icons/star.png";

const hardCodeImage = 'https://t1.rg.ltmcdn.com/es/images/0/1/9/img_huevos_fritos_con_bacon_6910_600.jpg'

export default function DetailsScreen(props) {

  function BackPress(){
    props.navigation.navigate('Select');
  }

  return (
    <Container>
      <View style={styles.container}>
        <View style={styles.background}>
          <Text style={{
            marginRight: '5%',
            marginTop: '8%',
            width: '92%',
            fontSize: 14,
            color: '#fff',
            fontFamily: 'comfortaa-bold',
            textAlign: 'right'
          }}>{'SMALL BITES'}</Text>
          <View style={{
            alignItems: 'flex-start',
            width: screenWidth,
            height: screenWidth * 0.7,
            flexDirection: 'row',
            marginTop: '2%',
          }}>
            <View style={{width: '10%', height: screenWidth * 0.7, alignItems: 'center', justifyContent: 'center'}}>
              <View style={{ marginBottom: GetWidth(6), borderRadius: GetWidth(10), width: GetWidth(7), height: GetWidth(7), backgroundColor: '#fff' }} />
              <View style={{ marginBottom: GetWidth(6), borderRadius: GetWidth(10), width: GetWidth(6), height: GetWidth(6), backgroundColor: 'rgba(255,255,255,0.82)' }} />
              <View style={{ borderRadius: GetWidth(10), width: GetWidth(6), height: GetWidth(6), backgroundColor: 'rgba(255,255,255,0.82)' }} />
            </View>
            <View style={{ alignItems: 'flex-end'}}>
              <Image source={{uri: hardCodeImage}} style={{width: screenWidth * 0.9, height: screenWidth * 0.7, resizeMode: 'contain'}} />
            </View>
          </View>
          <Text style={{fontSize: 18, fontFamily: 'comfortaa-bold', color: '#fff', marginLeft: '10%', marginTop: '10%'}}>Egg and Bacon</Text>
          <Text style={{fontSize: 16, fontFamily: 'comfortaa-bold', color: '#fff', marginLeft: '10%', marginTop: '10%', width: '80%'}}>The Egg and Bacon in the breakfast is the best for start the day</Text>
          <View style={styles.searchBarStarContainer}>
            <TouchableOpacity style={{ width: screenWidth * 0.8, justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{fontSize: 18, fontFamily: 'comfortaa-bold', color: '#fff'}}>Call for princing</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.starContainer} activeOpacity={1}>
              <Image source={StarIcon} style={styles.starIcon} />
            </TouchableOpacity>
          </View>
          <TouchableOpacity onPress={BackPress} style={{ position: 'absolute', top: screenHeight * 0.03, left: screenWidth * 0.05 }} >
            <Text style={{ fontSize: 20, color: '#fff', fontWeight: 'bold' }} >BACK</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Container>
  );
}

DetailsScreen.navigationOptions = {
  title: 'Links',
};

const styles = StyleSheet.create({
  container: {
    width: screenWidth,
    height: screenHeight,
    backgroundColor: '#fff',
    paddingTop: getStatusBarHeight(),
    justifyContent: 'center',
    alignItems: 'center'
  },
  background: {
    height: screenHeight - getStatusBarHeight() - screenHeight * 0.01,
    width: screenWidth - screenWidth * 0.02,
    borderRadius: screenWidth * 0.1,
    backgroundColor: '#bfbfbf',
    overflow: 'hidden',
    position: 'relative'
  },
  starIcon: {
    width: GetWidth(30),
    height: GetWidth(30),
    resizeMode: 'contain'
  },
  starContainer: {
  },
  searchBarStarContainer: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    position: 'absolute',
    bottom: GetHeight(30),
    left: 0
  },
});
