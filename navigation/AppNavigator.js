import React from 'react';
import { Platform } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import {
  Animated, Easing,
} from 'react-native';
import TabBarIcon from '../components/TabBarIcon';
import Menus from '../screens/MenusScreen';
import Details from '../screens/DetailsScreen';
import Invite from '../screens/InviteScreen';
import {SelectScreen} from "../screens/selectScreen";

const config = Platform.select({
  web: { headerMode: null },
  default: {},
});

const MenusStack = createStackNavigator(
  {
    Menus: Menus,
  },
  {
    headerMode: 'none',
    transitionConfig: () => ({
      transitionSpec: {
        duration: 0,
        timing: Animated.timing,
        easing: Easing.step0
      }
    }),
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);

MenusStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-information-circle'
      }
    />
  ),
};

Menus.path = '';

const DetailsStack = createStackNavigator(
  {
    Links: Details,
  },
  {
    headerMode: 'none',
    transitionConfig: () => ({
      transitionSpec: {
        duration: 0,
        timing: Animated.timing,
        easing: Easing.step0
      }
    }),
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);

DetailsStack.navigationOptions = {
  tabBarLabel: 'Links',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-link' : 'md-link'} />
  ),
};

DetailsStack.path = '';

const InviteStack = createStackNavigator(
  {
    Invite: Invite,
  },
  {
    headerMode: 'none',
    transitionConfig: () => ({
      transitionSpec: {
        duration: 0,
        timing: Animated.timing,
        easing: Easing.step0
      }
    }),
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);

InviteStack.navigationOptions = {
  tabBarLabel: 'Settings',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-options' : 'md-options'} />
  ),
};

InviteStack.path = '';


const AppNavigator = createAppContainer(createStackNavigator({
    Menus: { screen: MenusStack },
    Details: { screen: DetailsStack },
    Invite: { screen: InviteStack },
    Select: { screen: SelectScreen }
  },
  {
    initialRouteName: 'Select',
    headerMode: 'none',
    transitionConfig: () => ({
      transitionSpec: {
        duration: 0,
        timing: Animated.timing,
        easing: Easing.step0
      }
    }),
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
));

export default AppNavigator;
// export default tabNavigator;
